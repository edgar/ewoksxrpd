ewoksxrpd |version|
===================

*ewoksxrpd* provides data processing workflows for SAXS/WAXS.

*ewoksxrpd* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.eu/>`_.

Documentation
-------------

.. toctree::
    :maxdepth: 2

    pyfai
    api
