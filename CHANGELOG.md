# CHANGELOG.md

## 0.8.0 (unreleased)

## 0.7.0

New features:

- Add support for `pyFAI 2024.1`
- Add `monitor_uri ` to the output of `SumBlissScanImages` and `SumImages` tasks
- Improve error messages when no data is processed

Bug fixes:

- Fix project URLs in package metadata
- Handle relative file paths in `IntegrateBlissScan` and `SumBlissScanImages`
- Allow missing energy/wavelength in `PyFaiConfig` task

## 0.6.1

Bug fixes:

- remove print during integration

## 0.6.0

New features:

- Add `Integrate1DList`: task to integrate a list of images
- Add `SumBlissScanImages`: add diffraction patterns
- Add custom dark/flat correction (both raw counts)

## 0.5.0

New features:

- Support dark and flat from previous tasks

## 0.4.0

New features:

- `IntegrateBlissScan`: optionally use a master file (not open during processing) and a data file (open during processing)
- Integration tasks: argument to supply monitor and reference values
- Add new task to sum images `SumImages`
- Add new task to save images to EDF/TIFF `SaveImages`

Bug fixes:

- Handle numpy string when parsing units
- `IntegrateBlissScan`: Link to raw data are now in the master file

## 0.3.0

New features:

- Lima data from memory

## 0.2.0

New features:

- Azimuthal integration of a bliss scan without saving
- Error model support
- HDF5 saving: NXprocess as default is optional
- Integration tasks: additional parameter for integration options,
  to be used to overwrite the options coming from
  the configuration task
- Diagnostic tasks: support square-root scale for 1D plots
- Support pyFAI's "detector_config"
- Integration tasks: argument to change the worker pool size

Bug fixes:

- respect integration option "do_mask"

## 0.1.0

New features:

- Single and multi distance calibration
- Azimuthal integration of a single image
- Azimuthal integration of a bliss scan (during or after acquisition)
- Background subtraction
- Mask detection
- Save azimuthal integration results in HDF5
- Save azimuthal integration results in ASCII
- Azimuthal integration worker persistent in
  the process for each configuration
